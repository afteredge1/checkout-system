**Checkout System**

Company S is starting a computer store. You have been engaged to build the checkout system.
We will start with the following products in our catalogue:<br>

| SKU | Name | Price |
| ------ | ------ | ------ |
| ipd | Super iPad | $549.99 |
| mbp | MacBook Pro | $1399.99 |
| atv | Apple TV | $109.50 |
| vga | VGA adapter | $30.00|

As we’re launching our new computer store, we would like to have a few opening day specials.


1. We’re going to have a 3 for 2 deal on Apple TVs. For example, if you buy 3 units of
Apple TVs, you will only pay for the price of 2 units.


2. The brand new Super iPad will have a bulk discount applied, where the price will drop to
$499.99 each, if someone buys more than 4 units.

3. We will bundle in a VGA adapter free of charge with every MacBook Pro sold.

Our checkout system can scan items in any order.
