<?php
namespace CheckoutSystem\contracts;

interface CartInterface {
    public function AddPromotionalProduct($cart, $sku, $qty);
    public function UpdateCartItemPrice($cart, $price);
    public function AddCartDiscount($cart, $discount);
}
