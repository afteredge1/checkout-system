<?php
namespace CheckoutSystem;

class Promotion
{
    protected $promotion;
    public function __construct()
    {
        $this->promotion_rules = array(
            'ipd' => array(
                'qty' => 4,
                'discount_price' => '499.99',
                'discount_percent' => null,
                'additional_item' => null
            ),
            'atv' => array(
                'qty' => 3,
                'discount_price' => null,
                'discount_percent' => '33.33',
                'additional_item' => null
            ),
            'mbp' => array(
                'qty' => 1,
                'discount_price' => null,
                'discount_percent' => null,
                'additional_item' => 'vga'
            )
        );
    }
    public function Index($cart)
    {
        $promotion_item = null;
        $promotion = null;
        $final_cart = $cart;
        foreach ($cart as $key => $value) {
            $prod_sku = $value['sku'];
            $promotion_item = ($this->promotion_rules[$prod_sku]) ?? null;

            if (!is_null($promotion_item)) {
                $final_cart = $this->ApplyPromotion($cart, $promotion_item);
            }
        }

        return $final_cart;
    }

    public function ApplyPromotion($cart, $promotion_item)
    {
        $c = new Cart();
        $cart_qty = implode(array_column($cart, 'qty'));

        if ($cart_qty == $promotion_item['qty']) {
            if (!is_null($promotion_item['discount_price'])) {
                return $c->UpdateCartItemPrice($cart, $promotion_item['discount_price']);
            } else if (!is_null($promotion_item['discount_percent'])) {
                return $c->AddCartDiscount($cart, $promotion_item['discount_percent']);
            } else if (!is_null($promotion_item['additional_item'])) {
                return $c->AddPromotionalProduct($cart, $promotion_item['additional_item'], 1);
            }
        }

        return $cart;
    }
}
