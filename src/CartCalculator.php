<?php
namespace CheckoutSystem;

class CartCalculator
{
    public function CartTotal($cart, $discount = '')
    {
        $total = 0;
        foreach ($cart as $key => $value) {
            $total += ($value['price'] * $value['qty']);
        }

        return $total;
    }
}
