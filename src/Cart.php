<?php
namespace CheckoutSystem;

use CheckoutSystem\contracts\CartInterface;

class Cart implements CartInterface
{
    public function __construct()
    {
        $this->cart = array();
    }

    public function AddProduct($sku, $qty)
    {
        $item = array();
        $p = new Products();
        $product = $p->ProductBySku($sku);
        if ($product) {
            $item['sku'] = $sku;
            $item['title'] = $product['title'];
            $item['price'] = $product['price'];
            $item['qty'] = $qty;

            array_push($this->cart, $item);
        }

        return $this->cart;
    }

    public function AddPromotionalProduct($cart, $sku, $qty)
    {
        $p = new Products();
        $product = $p->ProductBySku($sku);
        if ($product) {
            $item['sku'] = $sku;
            $item['title'] = $product['title'];
            $item['price'] = '0.00';
            $item['qty'] = $qty;

            array_push($cart, $item);
        }

        return $cart;
    }

    public function UpdateCartItemPrice($cart, $price)
    {
        $cart[0]['price'] = $price;
        return $cart;
    }

    public function AddCartDiscount($cart, $discount)
    {
        $cost = $cart[0]['price'];
        $qty = $cart[0]['qty'];
        $total_cost = $cost * $qty;
        $cost = $total_cost * ($discount/100);
        $cart[0]['price'] = round(($total_cost - round($cost))/3, 2);
        return $cart;
    }
}
