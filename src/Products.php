<?php
namespace CheckoutSystem;

class Products
{
    public function __construct()
    {
        $this->product_list = array(
            0 => array(
                'sku' => 'ipd',
                'title' => 'Super IPad',
                'price' => '549.99'
            ),
            1 => array(
                'sku' => 'mbp',
                'title' => 'MacBook Pro',
                'price' => '1399.99'
            ),
            2 => array(
                'sku' => 'atv',
                'title' => 'Apple TV',
                'price' => '109.50'
            ),
            3 => array(
                'sku' => 'vga',
                'title' => 'VGA Adapter',
                'price' => '30.00'
            )
        );
    }
    public function ProductsList()
    {
        return $this->product_list;
    }

    public function ProductBySku($sku)
    {
        $array_index = null;
        foreach ($this->product_list as $key => $product) {
            if (in_array($sku, $product)) {
                $array_index = $key;
                break;
            }
        }
        if (is_null($array_index)) {
            return false;
        }

        return $this->product_list[$array_index];
    }
}
