<?php
namespace CheckoutSystem;

class Checkout
{
    public function Main()
    {
        $cart = new Cart();

        // add products to cart (one at a time for testing purpose)

        // sample product IPad
        // $checkout = $cart->AddProduct('ipd', 4);
        // $this->GetReceipt($checkout);

        // sample product Apple TV
        // $checkout = $cart->AddProduct('atv', 3);
        // $this->GetReceipt($checkout);

        // sample product Apple TV
        $checkout = $cart->AddProduct('mbp', 1);
        $this->GetReceipt($checkout);
    }

    public function GetReceipt($checkout)
    {
        $cc = new CartCalculator();
        $p = new Promotion();

        $promotion = $p->Index($checkout);
        $cart_total = $cc->CartTotal($promotion);

        // display loop
        echo "<hr>Cart Details<br>Sku_____________Title_____________Qty_____________Price<br><br>";
        foreach ($promotion as $key => $value) {
            echo "<br>".$value['sku']."_____________".$value['title']."_____________".$value['qty']."_____________$".$value['price']."";
        }
        echo "<br><br>Cart Total: $".round($cart_total)."<hr>";
    }
}
